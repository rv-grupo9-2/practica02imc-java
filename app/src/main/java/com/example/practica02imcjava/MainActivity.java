package com.example.practica02imcjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    // Declarar objetos del Main
    private EditText txtAlturaMain;
    private EditText txtPesoMain;
    private TextView lblIMCMain;
    private Button btnCalcularMain;
    private Button btnLimpiarMain;
    private Button btnCerrarMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Relación de objetos
        txtAlturaMain = (EditText) findViewById(R.id.txtAltura);
        txtPesoMain = (EditText) findViewById(R.id.txtPeso);
        lblIMCMain = (TextView) findViewById(R.id.lblIMC);
        btnCalcularMain = (Button) findViewById(R.id.btnCalcular);
        btnLimpiarMain = (Button) findViewById(R.id.btnLimpiar);
        btnCerrarMain = (Button) findViewById(R.id.btnCerrar);

        // Codificación del botón para calcular el IMC
        btnCalcularMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tomamos los textos de los EditText y los volvemos String
                String textoAltura = txtAlturaMain.getText().toString();
                String textoPeso = txtPesoMain.getText().toString();

                // Validar que los campos no estén vacíos
                if(textoAltura.isEmpty()) {
                    txtAlturaMain.setError("Campo requerido");
                } else if(textoPeso.isEmpty()) {
                    txtPesoMain.setError("Campo requerido");
                } else {
                    // Tomar los valores de peso y altura como tipo Double
                    double altura = Double.parseDouble(txtAlturaMain.getText().toString());
                    double peso = Double.parseDouble((txtPesoMain.getText().toString()));

                    // Validar que los valores no sean igual a 0
                    if(altura!=0 || peso!=0) {
                        // Cálculo de IMC (peso en kilogramos dividido por altura en metros al cuadrado)
                        // Se convierten los centímetros a metros
                        altura = altura/100;
                        double imc = peso / (altura * altura);

                        // Definir formato de 2 decimales
                        DecimalFormat df = new DecimalFormat("#.00");

                        // Mostrar el resultado del cálculo del IMC
                        lblIMCMain.setText("Su IMC es: " + df.format(imc) + " kg/m2");
                    }
                }
            }
        });

        // Codificación del botón para limpiar los EditText y el TextView del IMC
        btnLimpiarMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAlturaMain.getText().toString().matches("") &&
                    txtPesoMain.getText().toString().matches("") &&
                    lblIMCMain.getText().toString().matches("Su IMC es:")) {
                        Toast.makeText(MainActivity.this,
                                "No hay información para limpiar",
                                Toast.LENGTH_SHORT).show();
                } else {
                    txtAlturaMain.setText("");
                    txtPesoMain.setText("");
                    lblIMCMain.setText("Su IMC es:");
                }
            }
        });

        // Codificación del botón para cerrar la aplicación
        btnCerrarMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}